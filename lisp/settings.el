
;; Always use spaces instead of tabs for indentation
(setq-default indent-tabs-mode nil)

;; Show the current line and column number
(line-number-mode 1)
(column-number-mode 1)

;; Get rid of the startup screen
(setq inhibit-startup-message t)

;; Put customizations in their own file
(setq custom-file "~/.emacs.d/custom.el")
(load custom-file)

(provide 'settings)
