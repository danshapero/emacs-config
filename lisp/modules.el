
(require 'util)

(defvar modules
  '(
    ;; Parenthesis & delimiter balancing
    smartparens

    ;; Visual guide to indent level
    indent-guide
    )
  )

(get-packages modules)

;; Enable all of the assorted modules
(smartparens-global-mode t)
(indent-guide-global-mode)

(provide 'modules)
