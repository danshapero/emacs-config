
(require 'util)

(defvar color-themes
  '(
    zenburn-theme
    molokai-theme
    )
  )

(get-packages color-themes)

(provide 'themes)
