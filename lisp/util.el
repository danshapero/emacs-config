
(defun sym+= (sym str)
  (intern (concat (symbol-name sym) str)))

(defun get-packages (package-list)
  (dolist (p package-list)
    (when (not (package-installed-p p))
      (package-install p))
    (require p)))

(provide 'util)
