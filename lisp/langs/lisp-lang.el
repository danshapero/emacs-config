
(defvar lisp-packages
  '(slime))

(defun lisp-config ()
  (load (expand-file-name "~/programs/quicklisp/slime-helper.el"))
  (setq inferior-lisp-program "sbcl"))

(provide 'lisp-lang)
