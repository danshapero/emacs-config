
(require 'util)

(defvar languages
  '(python
    c-c++
    lisp))

;; For each programming language in the list,
(dolist (language languages)
  (let ((lang-name (sym+= language "-lang")))
    (require lang-name)
    ;; get the list of packages that the language uses and install them,
    ;; and run any configuration, e.g. set indent style, choose which
    ;; interpreter, etc.
    (let ((package-list (symbol-value (sym+= language "-packages")))
          (config-function (sym+= language "-config")))
      (get-packages package-list)
      (funcall config-function))))

(provide 'langs)
