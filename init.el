
;; Emacs package management
(require 'package)

(push '("marmalade" . "http://marmalade-repo.org/packages/")
      package-archives)
(push '("melpa" . "http://melpa.milkbox.net/packages/")
      package-archives)
(push '("tromey" . "http://tromey.com/elpa/")
      package-archives)

(package-initialize)

(when (not package-archive-contents)
  (package-refresh-contents))


;; Add the directory containing all the config scripts to the load path
(add-to-list 'load-path "~/.emacs.d/lisp/")
(let ((default-directory "~/.emacs.d/lisp/"))
  (normal-top-level-add-subdirs-to-load-path))

;; General settings
(require 'settings)

;; Assorted useful modules, e.g. paren balancing, version control integration
(require 'modules)

;; Color themes
(require 'themes)

;; Language-specific modules; syntax highlighting, REPLs, etc.
(require 'langs)
